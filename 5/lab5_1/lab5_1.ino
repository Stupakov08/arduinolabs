int ledPin = 13;

int inPin = A1;

boolean val = 0;

void setup() {
  // put your setup code here, to run once:
 pinMode(ledPin, OUTPUT);
 pinMode(inPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly
  val = digitalRead(inPin);
  digitalWrite(ledPin, val);
}
