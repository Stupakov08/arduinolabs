int ledPin = 13;
int ledPin1 = 12;
int ledPin2 = 11;
int ledPin3 = 10;

int inPin = A2;
boolean var = 0;

void stan1(){
  digitalWrite(ledPin, LOW);
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, HIGH);
  digitalWrite(ledPin3, LOW);
  delay(900);
}

void stan2(){
  digitalWrite(ledPin, HIGH);
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  delay(500);
}

void stan3(){
  digitalWrite(ledPin, HIGH);
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, HIGH);
  delay(500);
}

void stan4(){
  digitalWrite(ledPin, LOW);
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, HIGH);
  digitalWrite(ledPin3, HIGH);
  delay(500);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(inPin, INPUT);

}
void right(){
  stan1();
  stan2();
  stan3();
  stan4();
}
void left(){
  stan4();
  stan3();
  stan2();
  stan1();
}
void loop() {
  var = digitalRead(inPin);
  
  if(var == 0){
    right();
  }
  else{
    left();
  }
};

