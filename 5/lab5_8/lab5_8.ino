int ledPin = 13;
int inPin = 9;
int ledPin2 = 3;
boolean val = 0;
#define on LOW
#define off HIGH

void setup() {
 Serial.begin(9600);
 pinMode(ledPin, OUTPUT);
 pinMode(inPin, INPUT);
 pinMode(ledPin2, OUTPUT);
}

void loop() {
  val = digitalRead(inPin);
  if(val == 0) {
     Serial.print("ALARM\n");    
       
    digitalWrite(ledPin, on);
    delay(400);
    digitalWrite(ledPin, off);  

    digitalWrite(ledPin2, on);
    delay(400);
    digitalWrite(ledPin2, off);
    }
  if(val == 1) {
    digitalWrite(ledPin, off); 
    digitalWrite(ledPin2, off);    
      }
}
