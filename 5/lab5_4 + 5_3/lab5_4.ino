#define on LOW
#define off HIGH
#define sign 120

int ledPin = 13;
int zoomerPin = 3;

int inPin = A1;

void signal(int n){
  if(n == 1){
   digitalWrite(ledPin, on);
   digitalWrite(zoomerPin, on);
  }else {
    digitalWrite(ledPin, off);
    digitalWrite(zoomerPin, off);
  }
}

void S() {
  signal(1);
  delay(sign);
  signal(0);
  delay(sign);
  signal(1);
  delay(sign);
  signal(0);
  delay(sign);
  signal(1);
  delay(sign);
  signal(0);
}

void O() {
  signal(1);
  delay(3 * sign);
  signal(0);
  delay(sign);
  signal(1);
  delay(3* sign);
  signal(0);
  delay(sign);
  signal(1);
  delay(3* sign);
  signal(0);
}

void sos() {
   S();
   delay(3*sign);
   O();
   delay(3*sign);
   S();
   delay(7*sign);
}
void setup() {
  // put your setup code here, to run once:
    pinMode(ledPin, OUTPUT);
    pinMode(zoomerPin, OUTPUT);
    pinMode(inPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sos();
}
